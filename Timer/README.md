# Timer

## Task Description
You need to implement a timer graphical desktop application using Qt framework and QML.

The timer must have the following functions:
- Setting of initial time.
- Starting of timer by pressing `Start` button.
- Pausing of timer by pressing `Stop` button.
- Reseting of timer by pressing `Reset` button.
- Saving of current timestamp to timestamps list by pressing `tap` button. Timestamps list should be displayed somewhere on the application window. Also the should appear scroll bar if list content is too large. Implement `TimestampsModel` and use it in qml as data source.
- Popup modal window should be displayed and alarm sound should be played after timeout. Press `OK` button to close popup window and stop alarm sound.
- Possibility to select desired alarm sound.
- Light/Dark mode, which can be instantly switched by some button.

Add logging of all important events(timer started, paused, reseted, ...)

## Important notes!
1. Do not implement the logic of the timer and logging in QML. This must be implemented in C ++ code. QML should be responsible only for the user interface, ie there should be an interaction between C++ and QML. All business logic should be implemented in C++ classes. The user interface should 'stretch' as the screen resizes. Try to make UI maximally user friendly.
2. You are not allowed to use qt designer, which is provided by Qt Creator for creating UI.
3. Use CMake build system for generating the project. Not QMake!!!
4. All resource files (sounds, qml files, etc...) should be included in `resources.qrc` and used in following way: `qrc:/...`. Do not use local path to any resource!!!
