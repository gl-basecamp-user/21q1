#ifndef LOGGER_H
#define LOGGER_H

#include <QString>

class Logger {
public:
    enum LogLevel{
        INFO,
        DEBUG,
        WARNING,
        CRITICAL
    };
    void logMessage(const QString& message, LogLevel level);
private:
    const QString LOG_FILE_NAME = "timer.log";
};

#endif // LOGGER_H
