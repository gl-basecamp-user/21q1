#ifndef TIMESTAMPS_MODEL_H
#define TIMESTAMPS_MODEL_H

#include <QAbstractListModel>
#include <QList>
#include <QString>

class TimestampsModel : public QAbstractListModel {
    Q_OBJECT
   public:
    enum TimestampsRole { Timestamp = Qt::DisplayRole };
    Q_ENUM(TimestampsRole)

    explicit TimestampsModel(QObject *parent = nullptr);

    int rowCount(const QModelIndex & = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    QHash<int, QByteArray> roleNames() const;

    void addTimestamp(const QString &timestamp);
    void clearModel();

   private:
    QList<QString> mTimestamps;
};

#endif // TIMESTAMPS_MODEL_H
